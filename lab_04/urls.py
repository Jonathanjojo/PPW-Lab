from django.urls import path
from . import views

app_name = "lab_04"

urlpatterns = [
    path('', views.home),
]