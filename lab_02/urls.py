from django.views.generic import RedirectView
from django.urls import path
from . import views

app_name = "lab_02"

urlpatterns = [
    path('wireframe/', RedirectView.as_view(url = 'https://wireframe.cc/xMB0ET')),
    path('prototype/', RedirectView.as_view(url = 'https://marvelapp.com/3da7f77/screen/47710285'))
]