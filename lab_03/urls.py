from django.urls import path
from . import views

app_name = "lab_03"

urlpatterns = [
    path('', views.home, name="home_page"),
    path('blog/', views.blog, name="blog"),
    path('signup/', views.signup, name="signup"),
]