from django.shortcuts import render

def home(request):
    response = {
        "title"     : "Welcome to My Page",
        "navs"      : {
            "Home"      : "#home",
            "Profile"   : "#profile",
            "Skills"    : "#skills",
            "Projects"  : "#projects",
            "Contacts"  : "#contacts",
            "Sign Up!"  : "signup"
        },
        "footer"    : "Copyright of Jonathanjojo"
    }
    return render(request, 'lab_03/home.html', response)

def blog(request):
    response = {
        "title"     : "Future Texts",
        "navs"      : {
        },
        "footer"    : ""
    }
    return render(request, 'lab_03/blog.html', response)

def signup(request):
    response = {
        "title"     : "Sign Me UP!",
        "navs"      : {
            "Home"      : "../",
            "Sign Up!"  : "#signup"
        },
        "footer"    : "Copyright of Jonathanjojo"
    }
    return render(request, 'lab_03/signup.html', response)