"""Lab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.home),
    path('admin/', admin.site.urls),
    path('lab_01/', include('lab_01.urls')),
    path('lab_02/', include('lab_02.urls')),
    path('lab_03/', include('lab_03.urls')),
    path('lab_04/', include('lab_04.urls')),
    path('lab_05/', include('lab_05.urls')),
]