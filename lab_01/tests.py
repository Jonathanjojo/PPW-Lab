from django.test import (
    Client,
    TestCase
)

from django.http import HttpRequest
from django.urls import resolve
from . views import calc_age, my_profile, dave_profile, ferriyal_profile
from datetime import date

class Lab_01UnitTest(TestCase):
    def test_page_is_exist(self):
        response = Client().get('/lab_01/')
        self.assertEqual(response.status_code,200)

    def test_calculate_age_is_correct(self):
        self.assertEqual(0, calc_age(date.today().year, 2018))
        self.assertEqual(19, calc_age(date.today().year, 1999))

    def test_using_profile_func(self):
        found = resolve('/lab_01/')
        self.assertEqual(found.func, my_profile)

    def test_using_dave_profile_func(self):
        found = resolve('/lab_01/dave_profile/')
        self.assertEqual(found.func, dave_profile)

    def test_using_ferriyal_profile_func(self):
        found = resolve('/lab_01/ferriyal_profile/')
        self.assertEqual(found.func, ferriyal_profile)

