from datetime import datetime, date
from django.shortcuts import render

# Create your views here.

current_year = int(datetime.now().strftime("%Y"))

def my_profile(request):
    name = 'Jonathan Christopher Jakub'
    nickname = 'Jojo'
    npm = '1706040151'
    university = 'Faculty of Computer Science at University of Indonesia'
    hobby = 'Making fun out of everything'
    birth_date = date(1999,1,1)
    
    response = {
        "name"          : name,
        "nickname"      : nickname,
        "npm"           : npm,
        "university"    : university,
        "hobby"         : hobby,
        "age"           : calc_age(current_year, birth_date.year)
    }
    return render(request, 'lab_01/profile.html', response)

def dave_profile(request):

    response = {
        "name"          : "Dave Nathanael",
        "nickname"      : "Dave",
        "npm"           : "1706040076",
        "university"    : "Faculty of Computer Science at University of Indonesia",
        "hobby"         : "Playing Music",
        "age"           : calc_age(current_year, date(1999, 5, 8).year)
    }

    return render(request, 'lab_01/dave.html', response)


def ferriyal_profile(request):

    response = {
        "name"          : "Firriyal Bin Yahya",
        "nickname"      : "Firriyal",
        "npm"           : "1706979240",
        "university"    : "Faculty of Computer Science at University of Indonesia",
        "hobby"         : "Ngoding",
        "age"           : calc_age(current_year, date(1999, 7, 17).year)
    }

    return render(request, 'lab_01/ferriyal.html', response)

def calc_age(current_year, birth_year):
    return current_year - birth_year if birth_year <= current_year else 0
