from django.urls import path
from . import views

app_name = "lab_01"

urlpatterns = [
    path('', views.my_profile, name="my_profile"),
    path('dave_profile/', views.dave_profile, name="dave_profile"),
    path('ferriyal_profile/', views.ferriyal_profile, name="ferriyal_profile")
]