# Joe-Lab

Jonathan's PPW Lab Projects 2018 

## URL

These lab projects can be accessed from [https://ppw-b-the-web-joe.herokuapp.com](https://ppw-b-the-web-joe.herokuapp.com)

## Contents

* [Lab_01](https://ppw-b-the-web-joe.herokuapp.com/lab_01) 
* [Lab_02](https://ppw-b-the-web-joe.herokuapp.com/lab_02) 
* [Lab_03](https://ppw-b-the-web-joe.herokuapp.com/lab_03)
* [Lab_04](https://ppw-b-the-web-joe.herokuapp.com/lab_04)  
* [Lab_05](https://ppw-b-the-web-joe.herokuapp.com/lab_05)

## Authors

* **Jonathan Christopher Jakub** - [Jonathanjojo](https://gitlab.com/Jonathanjojo)

## Acknowledgments

* PPW CSUI 2018
* Lecturer : Bu Maya Retno