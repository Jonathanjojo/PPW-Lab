from django import forms
from . import models
from .utils.Categories import CATEGORY_CHOICES

class CreateActivity(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        "class"         : "fields",
        "required"      : True,
        "placeholder"   : "Activity Name"
    }))
    date = forms.DateField(widget=forms.DateInput(attrs={
        "class"         : "fields",
        "required"      : True,
        "placeholder"   : "Save the Date : yyyy-MM-dd"
    }))
    start_time = forms.TimeField(widget=forms.TimeInput(attrs={
        "class"         : "fields",
        "required"      : True,
        "placeholder"   : "Start at : HH:mm:ss"
    }))
    end_time = forms.TimeField(widget=forms.TimeInput(attrs={
        "class"         : "fields",
        "required"      : True,
        "placeholder"   : "Finish by : HH:mm:ss"
    }))
    place = forms.CharField(widget=forms.TextInput(attrs={
        "class"         : "fields",
        "required"      : True,
        "placeholder"   : "Where should we go?"
    }))
    category = forms.ChoiceField(choices=CATEGORY_CHOICES, widget=forms.Select(attrs={
        "class"         : "fields",
        "required"      : True,
        "placeholder"   : "Category"
    }))
    
    class Meta:
        model = models.Activity

class CreateNote(forms.ModelForm):
    class Meta:
        model = models.Note
        fields = [
            "text",
        ]