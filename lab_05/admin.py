from django.contrib import admin

from .models import Activity, Note 

admin.site.register(Activity)
admin.site.register(Note)