import calendar
from django.db import models
from django import forms
from django.forms.fields import ChoiceField
from .utils.Categories import CATEGORY_CHOICES

class Activity(models.Model):
    name = models.CharField(max_length = 100)
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    place = models.CharField(max_length = 100)
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=10)

    def __str__(self):
        return self.name
    
    def day(self):
        return calendar.day_name[self.date.weekday()]

class Note(models.Model):
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, related_name='notes')
    text = models.CharField(max_length=100)

    def __str__(self):
        return self.text