from django.urls import path
from . import views

app_name = "lab_05"

urlpatterns = [
    path('', views.activity_list, name="activity_list"),
    path('activity_create/', views.activity_create, name="activity_create"),
    path('activity/<act_id>/', views.activity_details, name="activity_details"),
    path('activity/<act_id>/delete', views.activity_delete, name="activity_delete"),
    path('activity/<act_id>/note/', views.note_create, name="note"),
    path('activity_delete_all/', views.activity_delete_all, name="activity_delete_all")
]