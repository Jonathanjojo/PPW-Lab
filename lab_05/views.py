from django.shortcuts import render, redirect
from . import forms
from .models import Activity
import datetime, time

def activity_list(request):
    activities = Activity.objects.all().order_by('date', 'start_time')
    for activity in activities:
        if check_expire(activity):
            activity.delete()
    response = {
        "title"     : "Activity List",
        "navs"      : {
            "Create New!"  : 'activity_create/',
            "Activity List": "#activity_list",
        },
        "footer"    : "Copyright of Jonathanjojo",
        "activities": activities
    }
    return render(request, 'lab_05/activity_list.html', response)

def activity_create(request):
    if request.method == "POST":
        form = forms.CreateActivity(request.POST)
        if form.is_valid(): 
            activity = Activity()
            activity.name = form.cleaned_data['name']
            activity.place = form.cleaned_data['place']
            activity.date = form.cleaned_data['date']
            activity.start_time = form.cleaned_data['start_time']
            activity.end_time = form.cleaned_data['end_time']
            activity.category = form.cleaned_data['category']
            if date_is_valid(activity.date, activity.start_time, activity.end_time):
                activity.save()
            return redirect('lab_05:activity_list')
    else:
        form = forms.CreateActivity()
    response = {
        "title"     : "Explore and Act!",
        "navs"      : {
            "Home"      : '../',
            "Act!"  : "#activity_create"
        },
        "footer"    : "Copyright of Jonathanjojo",
        "form"      : form
    }
    return render(request, 'lab_05/activity_create.html', response)

def date_is_valid(date, start_time, end_time):
    return start_time < end_time and date >= datetime.date.today()

def check_expire(activity):
    return datetime.date.today() > activity.date
        
def activity_details(request, act_id):
    activity = Activity.objects.get(id=act_id)
    if request.method == "POST":
        note_form = forms.CreateNote(request.POST)
        if note_form.is_valid(): 
            instance = note_form.save()
            return redirect('lab_05:activity_details', act_id=act_id)
    else:
        note_form = forms.CreateNote()
    response = {
        "title"     : "Here is your Activity!",
        "navs"      : {
            "Home"      : '../../',
            "Create New!": '../../activity_create/',
            "Activity"   : "#activity_details"
        },
        "footer"    : "Copyright of Jonathanjojo",
        "activity"  : activity,
        "note_form" : note_form,
    }
    return render(request, 'lab_05/activity_details.html', response)

def activity_delete(request, act_id):
    activities = Activity.objects.get(id=act_id)
    activities.delete()
    return redirect('lab_05:activity_list')

def activity_delete_all(request):
    activities = Activity.objects.all()
    activities.delete()
    return redirect('lab_05:activity_list')

def note_create(request, act_id):
    activity = Activity.objects.get(id=act_id)
    if request.method == "POST":
        note_form = forms.CreateNote(request.POST)
        if note_form.is_valid(): 
            instance = note_form.save(commit=False)
            instance.activity = activity
            instance.save()
            return redirect('lab_05:activity_details', act_id=act_id)
    else:
        note_form = forms.CreateNote()